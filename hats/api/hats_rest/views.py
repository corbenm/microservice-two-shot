from django.shortcuts import render
from django.http import JsonResponse
from .models import Hats, LocationVO
import json
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods

# Create your views here.

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name"
    ]

class HatListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "styleName",
        "name",
        "id"
        ]
    def get_extra_data(self, o):
          return {"Location": o.location.closet_name}

class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id",
        "name",
        "fabric",
        "styleName",
        "color",
        "url",
        "location"
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }



@require_http_methods(["GET", "POST"])
def list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatDetailEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatListEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def show_hats(request, pk):
    if request.method == "GET":
        hat = Hats.objects.filter(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    # else:
    #     content = json.loads(request.body)
    #     try
