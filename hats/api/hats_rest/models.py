from django.db import models
from django.urls import reverse

# Create your models here.

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)

class Hats(models.Model):

    name = models.CharField(max_length=200)
    fabric = models.CharField(max_length=200)
    styleName = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    url = models.CharField(max_length=200)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name
