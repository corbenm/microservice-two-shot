# Wardrobify

Team:

* Person 1 Corben Morrison - Hats
* Person 2 Yohan Pak- Shoes


## Design

## Shoes microservice

I am going to create a model for my Shoes that has all my properties using encoders so that the information can be taken as json. Then I will pull using the poller to get my "bin" data and use it in my model.

## Hats microservice

I created two models a value object to be used as a foriegn key in the hat model. The value object got its info from the poller to the wardobe api.
