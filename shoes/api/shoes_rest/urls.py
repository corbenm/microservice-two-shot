from django.urls import path

from .views import (
    show_list_shoes,
    show_shoe
)


urlpatterns = [
    path("shoes/", show_list_shoes, name="show_list_shoes"),
    path("shoes/<int:pk>", show_shoe, name="show_shoe"),
]
