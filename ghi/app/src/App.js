import {useState, useEffect} from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList';
import Hatlist from './HatList';
import ShoeForm from './ShoeForm';
import HatForm from './HatForm';

function App(props) {
  const [ shoes, setShoes ] = useState([]);
  const [ hats, setHats ] = useState([]);

  async function getHats() {
    const response = await fetch('http://localhost:8090/api/hats/');
    if (response.ok) {
      const { hats } = await response.json();
      setHats(hats);
    } else {
      console.log("An error has occured while fetching the data")
    }
  }

  async function getShoes() {
    const response = await fetch('http://localhost:8080/api/shoes/');
    if (response.ok) {
      const { shoes } = await response.json();
      setShoes(shoes);
    } else {
      console.error('An error occurred fetching the data')
    }
  }

  useEffect(() => {
  getShoes();
  getHats();
  }, [])


  if (hats === undefined) {
    return null;
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/">
            <Route index element={<MainPage />} />
          </Route>
          <Route path="shoes">
            <Route index element={<ShoeList shoes={shoes} getShoes={getShoes}/>} />
            <Route path = "new" element={<ShoeForm getShoes={getShoes}/>}/>
          </Route>
          <Route path = "hats">
            <Route index element={<Hatlist hats={hats} getHats={getHats}/>} />
            <Route path = "new" element={<HatForm getHats={getHats}/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}
export default App;
