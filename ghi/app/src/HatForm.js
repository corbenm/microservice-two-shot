import React, { useState, useEffect } from 'react';

function HatForm({getHats}) {
const [locations, setLocations] = useState([]);
const [name, setName] = useState('');
const [fabric, setFabric] = useState('');
const [stylename, setStyleName] = useState('');
const [color, setColor] = useState('');
const [picture, setPicture] = useState('');
const [location, setLocation] = useState('');


const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
}
const handleFabricChange = (event) => {
    const value = event.target.value;
    setFabric(value);
}
const handleStyleChange = (event) => {
    const value = event.target.value;
    setStyleName(value);
}
const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
}
const handlePictureChange = (event) => {
    const value = event.target.value;
    setPicture(value);
}
const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
}

const fetchData = async () => {
    const locationUrl = 'http://localhost:8100/api/locations';

    const response = await fetch(locationUrl);

    if (response.ok) {
        const data = await response.json();
        setLocations(data.locations)
        console.log(locations);
    }
}
useEffect(() => {
    fetchData();
  }, []);

async function handleSubmit(event) {
    event.preventDefault();
    const data = {}
       data.name = name;
        data.fabric = fabric;
        data.styleName = stylename;
        data.color = color;
        data.url = picture;
        data.location = location;

    const hatUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
        const newHat = await response.json();
        console.log(newHat);
        setName('');
        setFabric('');
        setStyleName('');
        setColor('');
        setPicture('');
        setLocation('');
        getHats();
    }
}


return (
    // <p>I want a big hat?</p>
<div className="row">
<div className="offset-3 col-6">
<div className="shadow p-4 mt-4">
  <h1>Make a new hat</h1>
  <form onSubmit={handleSubmit} id="create-hat-form">
    <div className="form-floating mb-3">
      <input onChange={handleNameChange} placeholder="Name" value={name} required type="text" name="name" id="name" className="form-control" />
      <label htmlFor="name">Name</label>
    </div>
    <div className="form-floating mb-3">
      <input onChange={handleFabricChange} placeholder="Fabric" value={fabric} required type="text" name="fabric" id="fabric" className="form-control" />
      <label htmlFor="name">Fabric</label>
    </div>
    <div className="form-floating mb-3">
      <input onChange={handleStyleChange} placeholder="Stylename" value={stylename} required type="text" name="stylename" id="stylename" className="form-control" />
      <label htmlFor="name">Stylename</label>
    </div>
    <div className="form-floating mb-3">
      <input onChange={handleColorChange} placeholder="Color" value={color} required type="text" name="color" id="color" className="form-control" />
      <label htmlFor="name">Color</label>
    </div>
    <div className="form-floating mb-3">
      <input onChange={handlePictureChange} placeholder="Picture" value={picture} required type="text" name="picture" id="picture" className="form-control" />
      <label htmlFor="name">Picture Url</label>
    </div>
    <div className="mb-3">
      <select onChange={handleLocationChange} id="location" value={location} required name="location" className="form-select" >
      <option value="">Choose a location</option>
            {locations.map(location => {
            return (
            <option key={location.id} value={location.href}> {location.closet_name} </option>
            );
            })};
      </select>
    </div>
    <button className="btn btn-primary">Create</button>
  </form>
</div>
</div>
</div>
  );
}

export default HatForm;
