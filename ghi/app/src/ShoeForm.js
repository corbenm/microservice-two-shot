import React, { useState, useEffect } from 'react';

function ShoeForm({getShoes}) {
  const [name, setName] = useState('');
  const [manufacturer, setManufacturer] = useState('');
  const [color, setColor] = useState('');
  const [picture_url, setPicture] = useState('');
  const [bin, setBin] = useState('');
  const [bins, setBins] = useState([]);

  async function getBins() {
    const url = 'http://localhost:8100/api/bins/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    }
  }

  useEffect(() => {
    getBins();
  }, []);

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      name,
      manufacturer,
      color,
      picture_url,
      bin
    };
    console.log(data);
    const shoeUrl = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();
      console.log(newShoe);

      setName('');
      setManufacturer('');
      setColor('');
      setPicture('');
      setBin('');
      getShoes()
    }
  }

  function handleNameChange(event) {
    const { value } = event.target;
    setName(value);
  }

  function handleManufacturerChange(event) {
    const { value } = event.target;
    setManufacturer(value);
  }

  function handleColorChange(event) {
    const { value } = event.target;
    setColor(value);
  }

  const handlePictureChange = (event) => {
    setPicture(event.target.value);
  };

  function handleBinChange(event) {
    const { value } = event.target;
    setBin(value);
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new shoe</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={manufacturer} onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input value={picture_url} onChange={handlePictureChange} placeholder="Picture" required type="text" name="picture" id="picture" className="form-control" />
              <label htmlFor="picture">Picture</label>
            </div>
            <div className="mb-3">
              <select onChange={handleBinChange} placeholder="Bin" name="bin" id="bin" className={"form-select"}>
                    <option value="">Choose a bin</option>
                    {bins && bins.map(bin => {
                      return (
                        <option key={bin.id} value={bin.href}>
                            {bin.closet_name}</option>
                      )
                    })}
                  </select>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;
