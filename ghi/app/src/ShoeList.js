import React from 'react';

function ShoeList(props) {

  async function handleDelete(shoeId) {
    const deleteUrl = `http://localhost:8080/api/shoes/${shoeId}`;
    const fetchConfig = {
      method: 'DELETE',
    }
      const response = await fetch(deleteUrl, fetchConfig);
      if (response.ok) {
        await props.getShoes();
        console.log("Shoe has successfully been deleted");
      }
}

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Manufacturer</th>
          <th>Color</th>
          <th>Picture</th>
          <th>Bin</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {props.shoes.map((shoe) => (
            <tr key={shoe.id}>
              <td>{ shoe.name }</td>
              <td>{ shoe.manufacturer }</td>
              <td>{ shoe.color }</td>
              <td>
                  <img src={shoe.picture_url} alt="name" style={{width: '100px', height: 'auto'}} ></img>
              </td>
              <td>{ shoe.bin.closet_name } </td>
            <td>
              <button onClick={() => handleDelete(shoe.id)}>Delete</button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default ShoeList;
