import React, {useState} from "react";
function HatList(props) {

async function handleHatDeleted(hatId){
  await props.getHats();
}


  async function handleDelete(hatId) {
    const deleteUrl = `http://localhost:8090/api/hats/${hatId}`;
    const fetchConfig = {
      method: "DELETE"
    }
      const response = await fetch(deleteUrl, fetchConfig);
      if (response.ok) {
          // getHats();
        // window.location.reload(false);
        handleHatDeleted(hatId);
        console.log("Hat has sucessfully been deleted");
      }
  }
    // find a way to grab the id
    // throw it in a url http://localhost:8090/api/hats/${id} or something similar
    //basically run a handlesubmit function but with the method delete
    //console.log something

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Fabric</th>
            <th>Stylename</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Location</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {props.hats.map(hat => {
            return (
              <tr key={hat.id}>
                <td>{ hat.name }</td>
                <td>{ hat.fabric }</td>
                <td>{ hat.styleName }</td>
                <td>{ hat.color }</td>
                {/* <td>{ hat.url }</td> */}
                <td><img src={hat.url} alt="https://w7.pngwing.com/pngs/492/506/png-transparent-question-mark-template-question-mark-speech-balloon-explosion-question-mark-comics-text-logo.png" width="100" height="100" ></img></td>
                <td>{ hat.location.closet_name }</td>
                <td>
                  <button onClick={() => handleDelete(hat.id)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default HatList;
